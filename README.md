# MoonShine


Basically MoonShine is a very simple packet manager written as bash script.
I mainly use MacOs, so normally I use Homebrew for anything concerning packages, so why should i write my
own package manager?  
One reason would be simply because i want some training in writing bash scripts. The other one is, that there are some
programs for which there are no homebrew formulas and I am too lazy to write some.  
So as this is inspired by homebrew, its names are taken with a little nod to my main package manager.


## What does MoonShine?

MoonShine creates its own directory in which it will put all the downloaded files, compile or build them if necessary and will
create symbolic links to make them globally executable.
Besides that, it offers basic management options like listing all of the downloaded files, returning their path and of course removing them again.

## Requirements

Working bash and sudo.

## Installation

``` bash
git clone git@gitlab.com:touringtest48/moonshine.git
./moonshine distillery setup
```

## Uninstall

```bash
moonshine distillery destroy
```

You will be asked whether you want to delete all the other files too. Then the specified file(s) will be removed.

## Usage

## Configs

Customizeable variables are stored in the first few lines of the script.  

As default the working directory for MoonShine is ```~/.Shed```. You can change it to something like ```/usr/local/Shed```,  
but you have to remember, that the script needs to be called with sudo to work properly.  
```alias mnsh="sudo moonshine"``` or something like that would be a good workaround.  

As default the directory in which all the symlinks to the executables are linked is ```/usr/bin/```, this requries to execute sudo to install or remove data properly.  
If you want another directory to be used just change the value of ```LINK_DIR``` to the desired path, but don't forget to add it to the ```PATH``` variable in the rcfile of your shell.

## TODO

+ correct the install function (installation process itself)
+ implement destroy
+ add config files to the directories for info like whether installed with git, compiled or made with a Makefile
